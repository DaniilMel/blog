export const failFields = {
    ID: 'id',
    AUTHOR: 'author',
    AUTHORID: 'authorId',
    TAGS: 'tags',
    TEXT: 'text',
    LOCATION: 'coordinates',
    LIKES: 'likes',
    TIMER: 'time'
}

export const commentFields = {
    ID: 'id',
    AUTHORID: 'authorid',
    AUTHOR: 'authorname',
    TEXT: 'text',
    LIKES: 'likes',
}