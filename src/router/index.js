import Vue from "vue";
import Router from "vue-router";

import HomeView from "@/views/HomeView.vue";
import FailsView from "@/views/FailsView.vue";
import UserFailsView from "@/views/myFailsView.vue";
import ProfileView from "@/views/ProfileView.vue";
import SingleFailView from "@/views/SingleFailView.vue";
import EditFailView from "@/views/EditFailView.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/', redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/fails',
      name: 'fails',
      component: FailsView,
    },
    {
      path: '/fail/:id',
      name: 'fail',
      component: SingleFailView,
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: EditFailView,
    },
    {
      path: '/map',
      name: 'map',
      component: () =>
        import("@/views/MapView.vue")
    },
    {
      path: '/userfails/:username/:id?',
      name: 'userfails',
      params: true,
      component: UserFailsView,
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfileView,
      children: [
        {
          name: 'login',
          path: '/login',
          component: () =>
            import("@/components/loginForm.vue"),
        },
        {
          name: 'registration',
          path: '/registration',
          component: () =>
            import("@/components/registrationForm.vue"),
        },
        {
          name: 'profileView',
          path: ':name',
          component: () =>
            import("@/components/profileForm.vue"),
        }
      ]
    }

  ]
});
